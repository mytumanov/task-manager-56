package ru.mtumanov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.command.AbstractCommand;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class FileScanner {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        @NotNull final Iterable<AbstractCommand> abstractCommands = bootstrap.getCommandService().getCommandsWithArgument();
        abstractCommands.forEach(e -> this.commands.add(e.getName()));
        executorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory())
                continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    Files.delete(file.toPath());
                    bootstrap.processCommand(fileName);
                } catch (@NotNull Exception e) {
                    bootstrap.getLoggerService().error(e);
                }
            }
        }
    }

    public void stop() {
        executorService.shutdown();
    }

}
