package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Repository
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();

    @Override
    @NotNull
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public void add(@NotNull AbstractCommand abstractCommand) {
        @Nullable final String name = abstractCommand.getName();
        if (name != null && !name.isEmpty())
            mapByName.put(name, abstractCommand);
        @Nullable final String argument = abstractCommand.getArgument();
        if (argument != null && !argument.isEmpty())
            mapByArgument.put(argument, abstractCommand);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull String name) {
        if (name.isEmpty())
            return null;
        return mapByName.get(name);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@NotNull String arg) {
        if (arg.isEmpty())
            return null;
        return mapByArgument.get(arg);
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommandsWithArgument() {
        return mapByArgument.values();
    }

}
