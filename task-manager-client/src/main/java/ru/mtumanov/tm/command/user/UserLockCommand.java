package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.user.UserLockRq;
import ru.mtumanov.tm.dto.response.user.UserLockRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class UserLockCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "lock user";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-lock";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRq request = new UserLockRq(getToken(), login);
        @NotNull final UserLockRs response = getUserEndpoint().userLock(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
