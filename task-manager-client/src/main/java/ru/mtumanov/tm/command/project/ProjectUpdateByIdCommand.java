package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.project.ProjectUpdateByIdRq;
import ru.mtumanov.tm.dto.response.project.ProjectUpdateByIdRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Update project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-update-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRq request = new ProjectUpdateByIdRq(getToken(), id, name, description);
        @NotNull final ProjectUpdateByIdRs response = getProjectEndpoint().projectUpdateById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
