package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskUnbindFromProjectRq;
import ru.mtumanov.tm.dto.response.task.TaskUnbindFromProjectRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Unbind task from project";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-unbind-from-project";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTERE TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRq request = new TaskUnbindFromProjectRq(getToken(), taskId, projectId);
        @NotNull final TaskUnbindFromProjectRs response = getTaskEndpoint().taskUnbindFromProject(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
