package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.project.ProjectClearRq;
import ru.mtumanov.tm.dto.response.project.ProjectClearRs;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECTS CLEAR]");
        @NotNull final ProjectClearRq request = new ProjectClearRq(getToken());
        @NotNull final ProjectClearRs response = getProjectEndpoint().projectClear(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
