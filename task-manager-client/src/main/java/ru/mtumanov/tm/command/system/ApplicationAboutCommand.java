package ru.mtumanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-a";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show about program";
    }

    @Override
    @NotNull
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: " + getPropertyService().getAuthorName());
        System.out.println("e-mail: " + getPropertyService().getAuthorEmail());
    }

}
