package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.project.ProjectCompleteByIdRq;
import ru.mtumanov.tm.dto.response.project.ProjectCompleteByIdRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Complete project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-complete-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectCompleteByIdRq request = new ProjectCompleteByIdRq(getToken(), id);
        @NotNull final ProjectCompleteByIdRs response = getProjectEndpoint().projectCompleteById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
