package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.project.ProjectRemoveByIdRq;
import ru.mtumanov.tm.dto.response.project.ProjectRemoveByIdRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRq request = new ProjectRemoveByIdRq(getToken(), id);
        @NotNull final ProjectRemoveByIdRs response = getProjectEndpoint().projectRemoveById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
