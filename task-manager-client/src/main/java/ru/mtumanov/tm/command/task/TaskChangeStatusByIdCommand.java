package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskChangeStatusByIdRq;
import ru.mtumanov.tm.dto.response.task.TaskChangeStatusByIdRs;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.StatusNotSupportedException;
import ru.mtumanov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Change task status by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-change-status-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STAUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        if (status == null)
            throw new StatusNotSupportedException();
        @NotNull final TaskChangeStatusByIdRq request = new TaskChangeStatusByIdRq(getToken(), id, status);
        @NotNull final TaskChangeStatusByIdRs response = getTaskEndpoint().taskChangeStatusById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
