package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.data.DataJsonSaveFasterXmlRq;
import ru.mtumanov.tm.dto.response.data.DataJsonSaveFasterXmlRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to json file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-json";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveFasterXmlRs response = getDomainEndpoint().saveDataJsonFasterXml(new DataJsonSaveFasterXmlRq(getToken()));
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
