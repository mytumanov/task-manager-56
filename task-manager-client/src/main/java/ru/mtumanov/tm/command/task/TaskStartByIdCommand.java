package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskStartByIdRq;
import ru.mtumanov.tm.dto.response.task.TaskStartByIdRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Start task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRq request = new TaskStartByIdRq(getToken(), id);
        @NotNull final TaskStartByIdRs response = getTaskEndpoint().taskStartById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
