package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskShowByIdRq;
import ru.mtumanov.tm.dto.response.task.TaskShowByIdRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Show task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRq request = new TaskShowByIdRq(getToken(), id);
        @NotNull final TaskShowByIdRs response = getTaskEndpoint().taskShowById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        showTask(response.getTask());
    }

}
