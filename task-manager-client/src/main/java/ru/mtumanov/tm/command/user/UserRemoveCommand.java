package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.user.UserRemoveRq;
import ru.mtumanov.tm.dto.response.user.UserRemoveRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class UserRemoveCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "user remove";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-remove";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRq request = new UserRemoveRq(getToken(), login);
        @NotNull final UserRemoveRs response = getUserEndpoint().userRemove(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
