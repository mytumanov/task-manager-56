package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskClearRq;
import ru.mtumanov.tm.dto.response.task.TaskClearRs;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CLEAR]");
        @NotNull final TaskClearRq request = new TaskClearRq(getToken());
        @NotNull final TaskClearRs response = getTaskEndpoint().taskClear(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
