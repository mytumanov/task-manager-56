package ru.mtumanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserRq extends AbstractRq {

    @Nullable
    private String token;

    protected AbstractUserRq(@Nullable final String token) {
        this.token = token;
    }

}
