package ru.mtumanov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M entity) throws AbstractException;

    @NotNull
    M update(@NotNull M entity) throws AbstractException;

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    long getSize();

    void removeById(@NotNull String id);

    void remove(@NotNull M model);

    boolean existById(@NotNull String id);

    @NotNull
    EntityManager getEntityManager();

}
