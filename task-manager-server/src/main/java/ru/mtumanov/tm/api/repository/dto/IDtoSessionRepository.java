package ru.mtumanov.tm.api.repository.dto;

import ru.mtumanov.tm.dto.model.SessionDTO;

public interface IDtoSessionRepository extends IDtoUserOwnedRepository<SessionDTO> {

}
