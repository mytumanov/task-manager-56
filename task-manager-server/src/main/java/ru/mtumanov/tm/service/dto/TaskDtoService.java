package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.service.dto.IDtoTaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@Service
public class TaskDtoService extends AbstractDtoUserOwnedService<TaskDTO, IDtoTaskRepository> implements IDtoTaskService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @Override
    @NotNull
    protected IDtoTaskRepository getRepository() {
        return context.getBean(IDtoTaskRepository.class);
    }

    @Override
    @NotNull
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        if (projectId.isEmpty())
            return Collections.emptyList();
        if (userId.isEmpty())
            throw new UserIdEmptyException();

        return getRepository().findAllByProjectId(userId, projectId);
    }

    @Override
    @NotNull
    public TaskDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) throws AbstractException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);

        @NotNull final IDtoTaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    @NotNull
    public TaskDTO updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();

        @NotNull final IDtoTaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final TaskDTO task = repository.findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    @NotNull
    public TaskDTO changeTaskStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (userId.isEmpty())
            throw new UserIdEmptyException();

        @NotNull final IDtoTaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final TaskDTO task = repository.findOneById(userId, id);
        task.setStatus(status);
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

}
