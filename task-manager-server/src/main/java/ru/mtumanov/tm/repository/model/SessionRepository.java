package ru.mtumanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.api.repository.model.ISessionRepository;
import ru.mtumanov.tm.model.Session;

import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    private static final String USER_ID = "userId";

    @NotNull
    private static final String ID = "id";

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Session> sessions = findAll(userId);
        for (@NotNull final Session session : sessions)
            remove(session);
    }

    @Override
    @NotNull
    public List<Session> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = getEntityManager();
        return entityManager.createQuery("FROM Session p WHERE p.userId = :userId", Session.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @NotNull
    public List<Session> findAll(@NotNull final String userId, @NotNull final Comparator<Session> comparator) {
        @NotNull final EntityManager entityManager = getEntityManager();
        return entityManager.createQuery("FROM Session p WHERE p.userId = :userId ORDER BY p." + getComporator(comparator), Session.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @Nullable
    public Session findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = getEntityManager();
        return entityManager.createQuery("FROM Session p WHERE p.userId = :userId AND p.id = :id", Session.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM Session p WHERE p.userId = :userId", Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Session project = findOneById(userId, id);
        remove(project);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        try {
            findOneById(userId, id);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    public void clear() {
        @NotNull final List<Session> sessions = findAll();
        for (@NotNull final Session session : sessions)
            remove(session);
    }

    @Override
    @NotNull
    public List<Session> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        return entityManager.createQuery("FROM Session p", Session.class)
                .getResultList();
    }


    @Override
    @NotNull
    public List<Session> findAll(@NotNull final Comparator<Session> comparator) {
        @NotNull final EntityManager entityManager = getEntityManager();
        return entityManager.createQuery("FROM Session p ORDER BY p." + getComporator(comparator), Session.class)
                .getResultList();
    }

    @Override
    @Nullable
    public Session findOneById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = getEntityManager();
        return entityManager.find(Session.class, id);
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM Session p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final Session project = findOneById(id);
        remove(project);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        try {
            findOneById(id);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

}
